package control;

import model.Senha;
import exception.CorRepetidaException;
import exception.SegredoInvalidoException;
import exception.TamanhoInvalidoException;
import exception.TentativaInvalidaException;
import model.CoresResposta;
import model.CoresSenha;
import model.Resposta;
import java.util.Collections;

public class ControladorJogo {

	private /*@ spec_public @*/ Senha segredo;
	private /*@ spec_public non_null @*/ Senha tentativa;
	private /*@ spec_public @*/ int rodadaAtual;
	private /*@ spec_public @*/ int tamanhoDoSegredo;
	public static final int MAX_RODADAS = 10;

	/*@  public invariant segredo != null 
	  @  	&& rodadaAtual <= MAX_RODADAS && 0 <= rodadaAtual; 
	  @*/

	/*@ 
	  @ assignable this.segredo;
	  @	assignable this.tamanhoDoSegredo;
	  @	assignable this.tentativa;
	  @	assignable this.rodadaAtual; 
	  @ ensures rodadaAtual == 0;
	  @ ensures segredo != null;
	  @ ensures tentativa != null;
	  @ ensures tamanhoDoSegredo == Senha.DEFAULT_SIZE; 
	  @*/
	public ControladorJogo() {
		
		this.rodadaAtual = 0;
		segredo = new Senha();
		tentativa = new Senha();
		this.tamanhoDoSegredo = Senha.DEFAULT_SIZE;
	
		gerarSegredo();
	}
	
	/*@ public normal_behavior
	  @ 	requires segredo != null;
	  @		requires segredo.getSenha().size() > 0;
	  @		assignable this.segredo;
	  @		assignable this.tamanhoDoSegredo;
	  @		assignable this.tentativa;
	  @		assignable this.rodadaAtual;
	  @		ensures this.segredo != null;
	  @		ensures this.tamanhoDoSegredo == segredo.getSenha().size();
	  @		ensures this.tentativa != null;
	  @		ensures this.rodadaAtual == 0;
	  @ also
	  @ public exceptional_behavior
	  @		requires segredo == null || segredo.getSenha().size() > 0;
	  @		assignable this.segredo;
	  @		assignable this.tamanhoDoSegredo;
	  @		signals_only SegredoInvalidoException;
	  @		signals (SegredoInvalidoException) segredo == null ||
	   							segredo.getSenha().size() > 0;
	  @*/
	public ControladorJogo(Senha segredo) throws SegredoInvalidoException {
		if(!segredoEhValido(segredo))
			throw new SegredoInvalidoException();
		this.segredo = segredo;
		this.tamanhoDoSegredo = segredo.getSenha().size();
		try {
			tentativa = new Senha(this.segredo.getSenha().size());
		} catch (TamanhoInvalidoException e) {
			throw new SegredoInvalidoException();
		}
		this.rodadaAtual = 0;
	}
	
	/**
	 * Respons�vel pela checagem da corretude do segredo passado
	 * como par�metro em caso de uso do segundo contrutor (que est� neste 
	 * projeto apenas como ponto de flexibilidade).
	 * 
	 * @param segredoSetup � o segredo com qual o cliente
	 *  desse m�todo deseja iniciar o jogo (fazer o setup do jogo).
	 * @return Se esse segredo � v�lido.
	 */
	
	/*@
	  @  requires (segredoSetup == null) || (segredoSetup.getSenha().size() == 0);
	  @	 ensures \result == false;
	  @ also
	  @  requires (\exists int i, j; (0 <= i && i < segredoSetup.getSenha().size()) &&
	  									(0 <= j && j < segredoSetup.getSenha().size()); 
	  									i != j && segredoSetup.getSenha().get(i) == segredoSetup.getSenha().get(j)); 
	  @	 ensures \result == false;
	  @	also
	  @	 ensures \result == true;	
	  @*/
	private boolean segredoEhValido(Senha segredoSetup){
		if(segredoSetup == null || segredoSetup.getSenha().size() == 0)
			return false;
		for(int it=0; it < segredoSetup.getSenha().size(); it++)
			for(int it2=it+1; it2 < segredoSetup.getSenha().size(); it2++)
				if(it!=it2 && segredoSetup.getSenha().get(it) == segredoSetup.getSenha().get(it2))
					return false;
		return true;
	}
	
	
	/**
	 * m�todo encarregado de gerar os valores que devem ser adivinhados pelo jogador.
	 * axiomOfChoice() gera um valor aleat�rio com um dos valores do enum CoresSenha 
	 */
	
	private void gerarSegredo() {
		int preenchidos = 0;
		while(preenchidos != tamanhoDoSegredo)
			if(addNoSegredo(CoresSenha.axiomOfChoice()))
				preenchidos++;
	}

	/**
	 * adiciona um valor ao array de CoresSenha do objeto segredo
	 * 
	 * @param cor
	 * @return true caso tenha sido adicionado o valor ao array de CoresSenha do objeto segredo
	 */
	
	/*@  requires cor != null;
	  @  requires segredo.getSenha().contains(cor) == false;
	  @	 assignable segredo;
	  @  ensures \result == true;
	  @ also
	  @  requires cor != null;
	  @  requires segredo.getSenha().contains(cor) == true;
	  @  assignable \nothing;
	  @  ensures \result == false;
	  @*/
	private boolean addNoSegredo(CoresSenha cor) {
		if(!segredo.getSenha().contains(cor))
			return segredo.getSenha().add(cor);
		else return false;
	}
	
	
	/**
	 * adiciona um valor ao array de CoresSenha do objeto tentativa, caso esse valor 
	 * n�o tenha sido informado na mesma jogada
	 * 
	 * @param cor 
	 * @return true caso o valor tenha sido adicionado ao array de CoresSenha do objeto tentativa
	 * @throws CorRepetidaException
	 */
	
	/*@  public normal_behavior 
	  @  	requires cor != null;
	  @  	requires tentativa.getSenha().contains(cor) == false;
	  @		assignable tentativa;
	  @  	ensures \result == true;
	  @  also
	  @	 public exceptional_behavior
	  @		requires cor == null;
	  @		requires tentativa.getSenha().contains(cor)== true;
	  @		assignable \nothing;
	  @		signals_only CorRepetidaException;
	  @  	signals(CorRepetidaException) tentativa.getSenha().contains(cor) == true;
	  @*/
	public boolean addNaTentativa(CoresSenha cor) throws CorRepetidaException {
		if(!tentativa.getSenha().contains(cor))
			return tentativa.getSenha().add(cor);
		else throw new CorRepetidaException();
	}

	public /*@ pure @*/ Senha getTentativa() {
		return tentativa;
	}

	/*
	 * m�todo utilizado para verificar a validade da tentativa realizada pelo jogador 
	 */
	
	/*@ public normal_behavior	
	  @		requires tentativa != null;
	  @ 	requires tentativaEhValida(tentativa) == true;
	  @		assignable this.tentativa;
	  @		ensures this.tentativa != null;
	  @		ensures this.tentativa == tentativa;
	  @ also
	  @ public exceptional_behavior
	  @		requires (tentativa == null) || (tentativaEhValida(tentativa) == false);
	  @		assignable \nothing;
	  @		signals_only TentativaInvalidaException;		
	  @ 	signals(TentativaInvalidaException) tentativa == null || (tentativaEhValida(tentativa) == false); 
	  @*/
	public void setTentativa(Senha tentativa) throws TentativaInvalidaException {
		if(tentativa != null && tentativaEhValida(tentativa))
			this.tentativa = tentativa;
		else throw new TentativaInvalidaException();
	}
	
	
	/**
	 * m�todo que realmente verifica se os valores informados pelo jogador s�o v�lidos
	 * @param _tentativa objeto com o array de enums CoresSenha informado pelo jogador
	 * @return true caso a jogada seja v�lida e falso cc
	 */
	
	/*@  
	  @  ensures ((tentativa != null) && (\forall int i; 0 <= i  && i < tentativa.getSenha().size() &&
	  @		(\forall int n; i + 1 <= n  && n < (tentativa.getSenha().size()-1) &&
	  @ 		tentativa.getSenha().get(i) == tentativa.getSenha().get(n))) ==>
	  @   \result == false);
	  @*/
	private /*@ pure @*/ /*@ spec_public @*/ boolean tentativaEhValida(Senha _tentativa) {
		for(int it=0; it < _tentativa.getSenha().size(); it++)
			for(int it2=it+1; it2 < _tentativa.getSenha().size(); it2++)
				if(it != it2 && _tentativa.getSenha().get(it) == _tentativa.getSenha().get(it2))
					return false;	
		return true;
	}
	
	public /*@ pure @*/ int getRodadaAtual() {
		return rodadaAtual;
	}

	/**
	 * Analisa a jogada e retorna um vetor de brancos e pretos.
	 * Se o jogador tiver estourado os turnos o metodo retorna null
	 * (poderia lancar GameOverException, mas exceptions nao devem 
	 * ser usadas dessa forma).
	 */

	/*@ public normal_behavior
	  @ 	requires this.segredo.getSenha().size() > 0;
	  @		assignable rodadaAtual;
	  @		ensures (\forall int i; 0 <= i && i < (segredo.getSenha()).size();
	  @ 			(segredo.getSenha()).get(i) == \old((segredo.getSenha()).get(i)));
	  @		ensures ((rodadaAtual < MAX_RODADAS) ==> (\result != null)) 
	  @				&& ((rodadaAtual >= MAX_RODADAS) ==> (\result == null));
	  @ also
	  @ public exceptional_behavior
	  @ 	requires !(this.segredo.getSenha().size() > 0);
	  @		assignable \nothing;
	  @		signals_only SegredoInvalidoException;
	  @		signals (SegredoInvalidoException) !(this.segredo.getSenha().size() > 0);
	  @*/
	public /*@ nullable @*/ Resposta jogar() throws SegredoInvalidoException{
		
		Resposta feedback;
		try {
			feedback = new Resposta(this.segredo.getSenha().size());
		} catch (TamanhoInvalidoException e) {
			throw new SegredoInvalidoException();
		}
		
		for(int it=0; it < segredo.getSenha().size(); it++)
			for(int it2=0; it2 < tentativa.getSenha().size(); it2++)
				if( (tentativa.getSenha().get(it2) == segredo.getSenha().get(it)) && (it == it2))
					feedback.getResposta().add(CoresResposta.PRETO);
				else if(tentativa.getSenha().get(it2) == segredo.getSenha().get(it))
					feedback.getResposta().add(CoresResposta.BRANCO);		
		/*@
		  @  assert rodadaAtual < 10;
		  @*/
		this.rodadaAtual++;
		
		Collections.shuffle(feedback.getResposta());
		
		return (rodadaAtual < MAX_RODADAS) ? feedback : null;
	}
	
	
}