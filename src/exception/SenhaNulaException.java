package exception;

@SuppressWarnings("serial")
public class SenhaNulaException extends Exception {

	public SenhaNulaException() {
		super("Senha nula n�o � aceita (Um erro pode ter ocorrido)!");
	}

	public SenhaNulaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SenhaNulaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SenhaNulaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SenhaNulaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
