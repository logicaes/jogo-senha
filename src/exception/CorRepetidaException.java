package exception;

@SuppressWarnings("serial")
public class CorRepetidaException extends Exception {

	public CorRepetidaException() {
		 super("� proibido adicionar cores repetidas em certas estruturas do jogo!");
	}

	public CorRepetidaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CorRepetidaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CorRepetidaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CorRepetidaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
