package exception;

@SuppressWarnings("serial")
public class ControladorNuloException extends Exception {

	public ControladorNuloException() {
		super("Objeto do tipo ControladorJogo foi passado como null!");
	}

	public ControladorNuloException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ControladorNuloException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ControladorNuloException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ControladorNuloException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
