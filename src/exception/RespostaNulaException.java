package exception;

@SuppressWarnings("serial")
public class RespostaNulaException extends Exception {

	public RespostaNulaException() {
		super("Resposta nula n�o � aceita!");
	}

	public RespostaNulaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RespostaNulaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RespostaNulaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RespostaNulaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
