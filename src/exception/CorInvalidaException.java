package exception;

@SuppressWarnings("serial")
public class CorInvalidaException extends Exception {

	public CorInvalidaException() {
		super("Cor passada como argumento n�o � v�lida!");
	}

	public CorInvalidaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CorInvalidaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CorInvalidaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CorInvalidaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
