package exception;

@SuppressWarnings("serial")
public class TamanhoInvalidoException extends Exception {

	public TamanhoInvalidoException() {
		super("Tamanho da senha/resposta inv�lido(deve ser maior que 0)!");
	}

	public TamanhoInvalidoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TamanhoInvalidoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TamanhoInvalidoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TamanhoInvalidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
