package exception;

@SuppressWarnings("serial")
public class SegredoInvalidoException extends Exception {

	public SegredoInvalidoException() {
		super("O segredo passado como par�metro � inv�lido!");
	}

	public SegredoInvalidoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SegredoInvalidoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SegredoInvalidoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SegredoInvalidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
