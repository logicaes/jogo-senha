package exception;

@SuppressWarnings("serial")
public class TentativaInvalidaException extends Exception {

	public TentativaInvalidaException() {
		 super("Uma tentativa n�o foi permitida pelo jogo(estava nula ou violava regras).");
	}

	public TentativaInvalidaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TentativaInvalidaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TentativaInvalidaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TentativaInvalidaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
