package exec;

import control.ControladorJogo;
import exception.ControladorNuloException;
import view.InterfaceTextual;

public class RunGame {

	public static void main(String[] args) {
		
		ControladorJogo jogo = new ControladorJogo();
		
		InterfaceTextual iu = null;
		
		try {
			iu = new InterfaceTextual(jogo); 
		} catch (ControladorNuloException e) {
			System.out.println("Problemas para executar o jogo... Abortando aplica��o.");
			return;
		}
		iu.fluxoDeJogo();
	}

}
