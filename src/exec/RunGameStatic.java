package exec;

import java.util.ArrayList;

import control.ControladorJogo;
import exception.ControladorNuloException;
import exception.SegredoInvalidoException;
import exception.SenhaNulaException;
import model.Senha;
import view.InterfaceTextual;
import model.CoresSenha;

public class RunGameStatic {

	public static void main(String[] args) {
		
		Senha senha = new Senha();
		
		ArrayList<CoresSenha> cores = new ArrayList<CoresSenha>();
		cores.add(CoresSenha.VERMELHO);
		cores.add(CoresSenha.AMARELO);
		cores.add(CoresSenha.VERDE);
		cores.add(CoresSenha.CINZA);
		
		try {
			senha.setSenha(cores);
		} catch (SenhaNulaException e2) {
			System.out.println("Problemas para executar o jogo... Abortando aplica��o.");
			return;
		}
		
		ControladorJogo jogo = null;
		try {
			jogo = new ControladorJogo(senha);
		} catch (SegredoInvalidoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		InterfaceTextual iu = null;
		
		try {
			iu = new InterfaceTextual(jogo);
		} catch (ControladorNuloException e) {
			System.out.println("Problemas para executar o jogo... Abortando aplica��o.");
			return;
		}
		iu.fluxoDeJogo();
	}

}
