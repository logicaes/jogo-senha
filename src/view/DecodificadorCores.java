package view;

import java.util.ArrayList;
import model.CoresResposta;
import model.CoresSenha;
import exception.CorInvalidaException;

public class DecodificadorCores {

	public DecodificadorCores() {}


	/*@ public normal_behavior
	  @	 assignable \nothing;
	  @  ensures \result != null;
	  @  ensures ((cor == CoresSenha.VERMELHO) ==> (\result == "Vermelho")) || ((cor == CoresSenha.AMARELO) ==> (\result == "Amarelo")) ||
	  @  ((cor == CoresSenha.CINZA) ==> (\result == "Cinza")) || ((cor == CoresSenha.LARANJA) ==> (\result == "Laranja"))||
	  @  ((cor == CoresSenha.ROSA) ==> (\result == "Rosa")) || ((cor == CoresSenha.ROXO) ==> (\result == "Roxo")) ||
	  @	 ((cor == CoresSenha.VERDE) ==> (\result == "Verde"));
	  @
	  @ also
	  @  public exceptional_behavior
	  @ 	requires (cor == null);
	  @		assignable \nothing;
	  @		signals_only CorInvalidaException;		
	  @ 	signals(CorInvalidaException) cor == null; 
	  @*/
	public String decodificar(CoresSenha cor) throws CorInvalidaException {
		switch(cor){
			case VERMELHO:
				return "Vermelho";
			case AMARELO:
				return "Amarelo";
			case CINZA:
				return "Cinza";
			case LARANJA:
				return "Laranja";
			case ROSA:
				return "Rosa";
			case ROXO:
				return "Roxo";
			case VERDE:
				return "Verde";
			default:
				throw new CorInvalidaException();
		}
	}
	
	/*@
	  @	ensures \result != null;
	  @ ensures (\exists CoresSenha x; x == \result);
	  @*/
	public CoresSenha decodificar(String cor) throws CorInvalidaException{
		switch(cor){
			case "vermelho":
				return CoresSenha.VERMELHO;
			case "amarelo":
				return CoresSenha.AMARELO;
			case "cinza":
				return CoresSenha.CINZA;
			case "laranja":
				return CoresSenha.LARANJA;
			case "rosa":
				return CoresSenha.ROSA;
			case "roxo":
				return CoresSenha.ROXO;
			case "verde":
				return CoresSenha.VERDE;
			default:
				throw new CorInvalidaException();
		}
	}
	
	public ArrayList<String> decodificarSenha(ArrayList<CoresSenha> senha) throws CorInvalidaException{
		ArrayList<String> cores = new ArrayList<String>();
		for(CoresSenha ph : senha){
			cores.add(this.decodificar(ph));
		}
		
		return cores;
	}
	
	/*@ requires cor != null;
	  @ ensures (cor == CoresResposta.BRANCO ==> \result == "Branco") &&
	  @ 		(cor == CoresResposta.PRETO ==> \result == "Preto");
	  @ signals(CorInvalidaException e) cor != CoresResposta.BRANCO && cor != CoresResposta.PRETO;
	  @*/
	public String decodificar(CoresResposta cor) throws CorInvalidaException{
		switch(cor){
			case BRANCO:
				return "Branco";
			case PRETO:
				return "Preto";
			default:
				throw new CorInvalidaException();
		}
	}
	
	/*@ requires resposta != null && resposta.size() > 0;
	  @ ensures (\forall int i; 0 <= i && i < resposta.size(); (\result).get(i).equals("Preto") || (\result).get(i).equals("Branco")); 
	  @*/
	public ArrayList<String> decodeResposta(ArrayList<CoresResposta> resposta) throws CorInvalidaException{
		ArrayList<String> cores = new ArrayList<String>();
		
		for(CoresResposta ph : resposta){
			cores.add(decodificar(ph));
		}
		
		return cores;
	}
}
