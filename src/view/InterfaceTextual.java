package view;

import java.util.ArrayList;
import java.util.Scanner;

import control.ControladorJogo;
import exception.ControladorNuloException;
import exception.CorInvalidaException;
import exception.SegredoInvalidoException;
import exception.TentativaInvalidaException;
import model.CoresResposta;
import model.Resposta;
import model.Senha;

public class InterfaceTextual {

	private /*@ spec_public @*/ ControladorJogo jogo;
	private /*@ spec_public @*/ DecodificadorCores decoder;
	Scanner input;
	
	/*@ public invariant jogo != null && decoder != null;
	  @
	  @*/
	public InterfaceTextual(ControladorJogo jogo) throws ControladorNuloException {
		if (jogo != null)
			this.jogo = jogo;
		else
			throw new ControladorNuloException();

		decoder = new DecodificadorCores();
	
		input = new Scanner(System.in);
		
	} 
	
	
	/*@
	  @  requires (resposta != null) && (resposta.getResposta().size() != 4);
	  @  ensures \result == false;
	  @ also
	  @  requires (\exists int i; 0 <= i && i < resposta.getResposta().size(); resposta.getResposta().get(i) != CoresResposta.PRETO);
	  @	 ensures \result == false;
	  @ also
	  @	 requires (resposta.getResposta().size() == 4) && 
	  				(\forall int i; 0 <= i && i < resposta.getResposta().size();
	   				(resposta.getResposta().get(i) == CoresResposta.PRETO));
	  @	 ensures \result == true;
	  @*/
	public boolean ganhouOJogo(Resposta resposta) {
		if (resposta.getResposta().size() != 4)
			return false;
			
		for (int it = 0; it < resposta.getResposta().size(); it++)
			if (resposta.getResposta().get(it) != CoresResposta.PRETO)
				return false;

		return true;
	}
	
	/*@ requires cores != null;
	  @	ensures \result != null;
	  @*/
	private String formatar(ArrayList<String> cores) {
		String formato = "";

		for (String s : cores) {
			formato += ("[" + s + "]");
		}

		return formato;
	}

	
	public void fluxoDeJogo() {
		boolean ehParaExecutar = false;

		System.out.println("-------------------------> JOGO SENHA!!!\n"
				+ "Cores dispon�veis: Vermelho, Rosa, Amarelo, Roxo, Verde, Cinza, Laranja(as entradas devem"
				+ " ser lower case).\n"
				+ "Voc� tem 10 chances de acertar uma sequ�ncia de 4 cores, cada tentativa sua ser�"
				+ " respondida com uma sequ�ncia de cores BRANCAS e PRETAS, que ter�o os seguintes " + "significados:"
				+ "\n-Branco: Cor certa na posi��o errada." + "\n-Preto: Cor certa na posi��o certa."
				+ "\n-------------------------> Boa sorte! :)");

		while (true) {
			/*@
			  @  assert jogo.getRodadaAtual() < 10;
			  @*/
			System.out.println("Fa�a a sua jogada:");
			System.out.println("Restam " + (10 - jogo.getRodadaAtual()) + " jogadas.");

			Senha tentativa = new Senha();
			while (tentativa.getSenha().size() != 4) {
				
				/*@
				  @ assert (input.nextLine().equals("vermelho") || input.nextLine().equals("amarelo") || input.nextLine().equals("laranja") ||
				  @ input.nextLine().equals("verde") || input.nextLine().equals("cinza") || input.nextLine().equals("rosa") ||
				  @ input.nextLine().equals("roxo"));
				  @*/
				String corPino = input.nextLine();
				//@ assert corPino == null;
				try {
					tentativa.getSenha().add(decoder.decodificar(corPino));
				} catch (CorInvalidaException e) {
					System.out.println("Cor inv�lida, certifique-se que � uma das cores informadas.");
				}
			}

			System.out.println("_________________________________________________________");

			ehParaExecutar = true;
			
			try {
				jogo.setTentativa(tentativa);
			} catch (TentativaInvalidaException e) {
				System.out.println("H� erros na sequ�ncia de cores escolhidas(repeti��o � pro�bida)! Tente novamente.");
				ehParaExecutar = false;
			}

			if (ehParaExecutar) {
				Resposta resposta = null;
				try {
					resposta = jogo.jogar();
				} catch (SegredoInvalidoException e) {
					System.out.println("Inconsist�ncia do jogo detectada... Abortando aplica��o.");
					return;
				}
				
				if (resposta == null) {
					System.out.println("Voc� perdeu, suas chances se esgotaram! :(");
					return;
				} else {
					try {
						System.out.println("Para a jogada " + formatar(decoder.decodificarSenha(tentativa.getSenha())));
					} catch (CorInvalidaException e) {
						System.out.println(
								"Cor inv�lida, certifique-se que � uma das cores informadas! Abortando aplica��o...");
						return;
					}

					try {
						System.out.println("A resposta foi " + formatar(decoder.decodeResposta(resposta.getResposta())));
					} catch (CorInvalidaException e) {
						System.out.println(
								"Cor inv�lida, certifique-se que � uma das cores informadas! Abortando aplica��o...");
						return;
					}

					if (ganhouOJogo(resposta)) {
						System.out.println("_________________________________________________________");
						System.out.println("Parab�ns, voc� venceu! :)");
						return;
					}
				}
			}
			System.out.println("_________________________________________________________");
		}
	}

}
