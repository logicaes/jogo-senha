package model;

import java.util.Random;

public enum CoresSenha {
	VERMELHO, ROSA, AMARELO, ROXO, VERDE, CINZA, LARANJA;
	
	/*@
	  @	ensures \result != null;
	  @ ensures (\exists CoresSenha x; x == \result);
	  @*/
	public static CoresSenha axiomOfChoice(){
	    CoresSenha[] cores = CoresSenha.values();
	    Random randomizador = new Random();
	    return cores[randomizador.nextInt(cores.length)];
	}
}
