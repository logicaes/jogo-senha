package model;

import java.util.ArrayList;

import exception.RespostaNulaException;
import exception.TamanhoInvalidoException;

public class Resposta {

	private /*@ spec_public non_null @*/ ArrayList<CoresResposta> resposta;

	public static final int DEFAULT_SIZE = 4;

	//@ public invariant (resposta != null) ==> ( 0 <= resposta.size()); 
	
	/*@
	  @ assignable resposta;
	  @ ensures resposta != null;
	  @*/
	public Resposta() {
		this.resposta = new ArrayList<CoresResposta>(DEFAULT_SIZE);
	}

	/*@
	  @ public normal_behavior
	  @		requires tamanhoResposta > 0;
	  @		assignable this.resposta;
	  @ 	ensures this.resposta != null;
	  @ also
	  @ public exceptional_behavior
	  @		requires tamanhoResposta <= 0;
	  @		assignable \nothing;
	  @		signals_only TamanhoInvalidoException;
	  @		signals (TamanhoInvalidoException) tamanhoResposta <= 0;
	  @*/
	public Resposta(int tamanhoResposta) throws TamanhoInvalidoException {
		if(tamanhoResposta > 0)
			this.resposta = new ArrayList<CoresResposta>(tamanhoResposta);
		else throw new TamanhoInvalidoException();
	}
	
	public /*@ pure @*/ ArrayList<CoresResposta> getResposta() {
		return resposta;
	}

	/*@
	  @ public normal_behavior
	  @		requires resposta != null;
	  @		assignable this.resposta;
	  @		ensures this.resposta == resposta;
	  @ also
	  @ public exceptional_behavior
	  @		requires resposta == null;
	  @		assignable \nothing;
	  @		signals_only RespostaNulaException;
	  @		signals (RespostaNulaException) this.resposta == \old(this.resposta);
	  @		
	  @*/
	public void setResposta(ArrayList<CoresResposta> resposta) throws RespostaNulaException {
		if(resposta != null)
			this.resposta = resposta;
		else throw new RespostaNulaException();
	}
}