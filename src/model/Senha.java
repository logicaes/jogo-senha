package model;

import java.util.ArrayList;

import exception.SenhaNulaException;
import exception.TamanhoInvalidoException;

public class Senha {

	private /*@ spec_public non_null @*/ ArrayList<CoresSenha> senha;

	public static final int DEFAULT_SIZE = 4;
	
	//@ public invariant (senha != null) ==> (0 <= senha.size());
	
	
	/*@
	  @ assignable senha;
	  @ ensures senha != null;
	  @*/
	public Senha() {
		this.senha = new ArrayList<CoresSenha>(DEFAULT_SIZE);
	}
	
	/*@
	  @ public normal_behavior
	  @		requires tamanhoSenha > 0;
	  @		assignable senha;
	  @		ensures senha.size() == tamanhoSenha;
	  @ 	ensures senha != null;
	  @ also
	  @ public exceptional_behavior
	  @		requires tamanhoSenha <= 0;
	  @		assignable \nothing;
	  @		signals_only TamanhoInvalidoException;
	  @		signals (TamanhoInvalidoException) tamanhoSenha <= 0;
	  @*/
	public Senha(int tamanhoSenha) throws TamanhoInvalidoException {
		if(tamanhoSenha > 0)
			this.senha = new ArrayList<CoresSenha>(tamanhoSenha);
		else throw new TamanhoInvalidoException();
	}
	
	/*@
	  @	ensures \result == senha;
	  @ ensures senha == \old(senha);
	  @*/
	public /*@ pure @*/ ArrayList<CoresSenha> getSenha() {
		return senha;
	}
	
	/*@
	  @ public normal_behavior
	  @		requires senha != null;
	  @		assignable this.senha;
	  @		ensures this.senha == senha;
	  @ also
	  @ public exceptional_behavior
	  @		requires senha == null;
	  @		assignable \nothing;
	  @		signals_only SenhaNulaException;
	  @		signals (SenhaNulaException) this.senha == \old(this.senha);
	  @		
	  @*/
	public void setSenha(ArrayList<CoresSenha> senha) throws SenhaNulaException {
		if(senha != null)
			this.senha = senha;
		else throw new SenhaNulaException();
	}
}